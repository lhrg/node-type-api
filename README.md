# Base Project Nodejs+Typescript

Just a simple project that can be used for anyone.

- Techs
  - Nodejs
  - Typescript
  - Mongoose(MongoDB)
  - Express

### Installation

Install the dependencies and devDependencies and start the server.

```console
$ git clone https://gitlab.com/lhrg/node-type-api.git
$ cd node-type-api
$ npm i
$ npm run dev
```
License
----

MIT
